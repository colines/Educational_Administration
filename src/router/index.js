import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/pages/login/Login'
import Home from '@/pages/home/Home'
import Announcement from '@/pages/announcement/Announcement'
import OnlineLeave from '@/pages/onlineLeave/OnlineLeave'
import RoleManage from '@/pages/permissionSetting/RoleManage'
import UserPermission from '@/pages/permissionSetting/UserPermission'
import ManagerSetting from '@/pages/admin/Administrator'
import TuneAplay from '@/pages/lessonAplay/TuneAplay'
import AddOrStopAplay from '@/pages/lessonAplay/AddOrStopAplay'
import LeaveRecord from '@/pages/leaveRecord/LeaveRecord'
import LeaveRecord2 from '@/pages/leaveRecord/LeaveRecord2'
import LeaveRecord3 from '@/pages/leaveRecord/LeaveRecord3'
import StuAttendanceCheck from '@/pages/stuAttendanceCheck/StuAttendanceCheck'
import TeaAttendanceCheck from '@/pages/teaAttendanceCheck/TeaAttendanceCheck'


Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Login',
      component: Login
    },
    {
      path: '/home',
      name: 'Home',
      component: Home,
      children:[
        {
          path: 'announcement',
          name: 'announcement',
          component: Announcement
        },
        {
          path: 'onlineLeave',
          name: 'onlineLeave',
          component: OnlineLeave
        },
        {
          path:'roleManage',
          name:'roleManage',
          component:RoleManage
        },
        {
          path:'userPermission',
          name:'userPermission',
          component:UserPermission
        },
        {
          path:'managerSetting',
          name:'managerSetting',
          component:ManagerSetting
        },
        {
          path:'tuneAplay',
          name:'tuneAplay',
          component:TuneAplay
        },
        {
          path:'addOrStopAplay',
          name:'addOrStopAplay',
          component:AddOrStopAplay
        },
        {
          path:'leaveRecord',
          name:'leaveRecord',
          component:LeaveRecord
        },
        {
          path:'leaveRecord2',
          name:'leaveRecord2',
          component:LeaveRecord2
        },
        {
          path:'leaveRecord3',
          name:'leaveRecord3',
          component:LeaveRecord3
        },
        {
          path:'stuAttendanceCheck',
          name:'stuAttendanceCheck',
          component:StuAttendanceCheck
        },
        {
          path:'teaAttendanceCheck',
          name:'teaAttendanceCheck',
          component:TeaAttendanceCheck,
        }

      ]
    }
  ]
})
